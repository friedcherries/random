#!/bin/sh
 
LIMIT_DOWN="${1}Kbytes/s"
LIMIT_UP="${2}Kbytes/s"
 
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root." 1>&2
   exit 1
fi
 
ipfw pipe 1 config bw $LIMIT_DOWN
ipfw pipe 2 config bw $LIMIT_UP
ipfw add 1 pipe 1 all from any to me
ipfw add 2 pipe 2 all from me to any
