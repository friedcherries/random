echo off
setlocal EnableDelayedExpansion

REM SQL Server Server\Instance
SET SERVER=MCULEY\SQLEXPRESS2008

REM SQL Server Database name
SET DATABASE=simpsons

REM As many queries as you want. Increment index by one
SET SQL[1]="SELECT * FROM PERSON"
SET SQL[2]="SELECT * FROM PERSON WHERE FIRST_NAME='Lisa'"
SET SQL[3]="SELECT * FROM PERSON WHERE FIRST_NAME='Bart'"

REM You can also remove the above SET statements and put
REM  all of your SQL array into a seperate batch file
REM  and "include" it with the call commaond:
REM  call C:\Users\mculey\sql-statements.bat

REM Query to start with
SET NUM=1

REM Total number of queries
SET MAX=3

:GO
echo Executing query: !SQL[%NUM%]!
sqlcmd -S %SERVER% -d %DATABASE% -Q !SQL[%NUM%]!

SET /a NUM=%NUM%+1
if %NUM% GTR %MAX% ( 
  GOTO :EXIT 
)

set /p go= "Proceed to next query Y/N? "
if %go% == Y (
  echo %NUM%
  GOTO :GO
) else (
  GOTO :EXIT
)

:EXIT
echo DONE!
