<?php
define('QUARTER', 25);
define('DIME', 10);
define('NICKEL', 5);
define('PENNY', 1);

function makeChange($amount) {
    $change = array(QUARTER, DIME, NICKEL, PENNY);
    
    $result = array('quarters' => 0, 'dimes' => 0, 
                    'nickels' => 0, 'pennies' => 0);

    $holder = $amount;
    $holder = ($holder - intval($holder)) * 100;
    
    foreach ($change as $coin) {
        switch ($coin) {
            case QUARTER:
                $key = 'quarters';
                break;
            case DIME:
                $key = 'dimes';
                break;
            case NICKEL:
                $key = 'nickels';
                break;
            case PENNY:
                $key = 'pennies';
                break;
        }

        $result[$key] = floor($holder/$coin);
        $holder = $holder % $coin;
    }

    return $result;
}

$f = fopen( 'php://stdin', 'r' );
echo "Enter your amount: $";
$amount = 0;

while( $amount = fgets( $f )) {
  $amount = trim($amount);
  if ($amount[0] == 'Q' or $amount[0] == 'q') {
    break;
  }

  if (!is_numeric($amount)) {
    echo "$amount is not a number!\n";
  } else {
    $results = makeChange($amount);
    echo 'You provided $', $amount, "\n";
    echo 'Change breakdown is:', "\n";
    echo "\tQuarters: " . number_format($results['quarters'], 0) . "\n";
    echo "\tDimes: " . number_format($results['dimes'], 0) . "\n";
    echo "\tNickels: " . number_format($results['nickels'], 0) . "\n";
    echo "\tPennies: " . number_format($results['pennies'], 0) . "\n\n";
    echo "Enter your amount: $";
  }
}

fclose( $f );
