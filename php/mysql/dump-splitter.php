<?php

function smasher ($start, $finish) {
  global $file;
  $sql = '';

  for ($y=$start-1; $y<$finish-1; $y++) {
    $sql .= $file[$y];
  }
 
  return $sql;
}

function getFilename($lineno, $prefix) {
  global $file;
  $filename = '';

  $table = array_pop(explode(' ', $file[$lineno]));
  $table = trim(str_replace('`', '', $table));
  return "{$prefix}{$table}.sql";
}

$sqlFile = (empty($argv[1]) ? '' : $argv[1]);
$filePrefix = (empty($argv[2]) ? '' : $argv[2]);

if (!file_exists($sqlFile)) {
  echo "Provided sql file {$sqlFile} does not exist!\nYou suck!\n\n";
  exit;
}

$file = file($sqlFile);
$ids = array();

foreach ($file as $key => $line) {
  if (strpos($line, 'Table structure for table') !== false) {
    $ids[] = $key;
  }
}

for ($x=0; $x<count($ids); $x++) {
  $filename = getFilename($ids[$x], $filePrefix);
  
  if (empty($ids[$x+1])) {
    $finish = count($file) - 1;
  } else {
    $finish = $ids[$x+1];
  }

  $sql = smasher($ids[$x], $finish);
  file_put_contents($filename, $sql);
}
