#!/usr/bin/env bash

EXT=$1

find . -name \*.${EXT} -exec ls -l {} \; | awk '{ s+=$5 } END { print s/1024/1024 }'
