#!/usr/bin/env bash

COMMITS=$1
declare -A gitters

if [[ -z $COMMITS ]]; then
	COMMITS=5
fi
GIT_OUT=`git log -n ${COMMITS} | grep commit | awk '{print $2}'`;
FILES=`git show --pretty="format:" --name-only ${GIT_OUT}`;

# Cycling over the files and stuffing them into the hash is to get rid of the dupes
for FILE in $FILES; 
do
	gitters[$FILE]=0;
done

for GIGGITY in ${!gitters[@]}; do
	echo "${GIGGITY#trunk\/}";
done
